/* PROGRAM:             main.c
   AUTHOR:              Nicholas Sturgeon
   DATE:                2018/11/10
   PURPOSE:             Main entry into the program
   LEVEL OF DIFFICULTY: 0
   CHALLENGES:          None
   HOURS SPENT:         2 minutes
*/

#include "menu.h"

/* Start the program and display the main menu. */
int main (void) {
  display_main_menu();
  return 0;
}
