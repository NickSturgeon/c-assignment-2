CC = gcc
FLAGS = -pedantic -Wall -w -ansi
FILES = menu.c area.c phone_book.c main.c
OUT = assignment2

build:
	$(CC) $(FLAGS) -o $(OUT) $(FILES)
	