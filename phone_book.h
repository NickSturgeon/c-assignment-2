#ifndef PHONE_BOOK_H
#define PHONE_BOOK_H 

struct phone_entry {
  char last_name[20];
  char first_name[20];
  struct area *area;
  long phone_number;
  struct phone_entry *next_phone_entry;
};

void add_phone_entry(void);
struct phone_entry *new_phone_entry(void);
void initialize_phone_list(struct phone_entry *);
void modify_phone_entry(struct phone_entry *);
void delete_phone_entry(struct phone_entry *);
struct phone_entry *find_phone_entry_by_number(void);
struct phone_entry *find_phone_entry_by_last_name(void);
int check_phone_number_length(long);
int check_area_in_use(short);
char *format_phone_number(short, long);
void display_phone_entries();

#endif
