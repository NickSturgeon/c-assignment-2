#ifndef MENU_H
#define MENU_H

void display_main_menu(void);
void display_modify_phone_entry_menu(void);
void display_delete_phone_entry_menu(void);
void display_delete_area_menu(void);
  
#endif
