/* PROGRAM:             menu.c
   AUTHOR:              Nicholas Sturgeon
   DATE:                2018/11/10
   PURPOSE:             Menus for the program
   LEVEL OF DIFFICULTY: 0
   CHALLENGES:          None
   HOURS SPENT:         20 minutes
*/

#include "menu.h"
#include "area.h"
#include "phone_book.h"
#include <stdio.h>

/* Display the main menu. Get user input for their choice
 * and take the appropriate action.
 */
void display_main_menu(void) {
  char choice;
  
  for(;;) {
    printf("\nChoose one of the following options:\n");
    printf("[1] Enter Area Information\n");
    printf("[2] Enter Phone Book Entry\n");
    printf("[3] Modify Existing Phone Book Entry\n");
    printf("[4] Delete Existing Phone Book Entry\n");
    printf("[5] Delete an Unused Area\n");
    printf("[Q] Quit\n");
  
    printf("\nEnter choice: ");
    scanf("%c", &choice);
    while (getchar() != '\n') continue;
    
    switch (choice) {
      case '1':
        add_area();
        break;
      case '2':
        add_phone_entry();
        break;
      case '3':
        display_modify_phone_entry_menu();
        break;
      case '4':
        display_delete_phone_entry_menu();
        break;
      case '5':
        display_delete_area_menu();
        break;
      case 'q':
      case 'Q':
        printf("Quitting...\n");
        return;
      default:
        printf("Invalid Option\n");
    }
  }
}

/* Display the menu to modify a phone entry. Get user input 
 * for their choice and take the appropriate action.
 */
void display_modify_phone_entry_menu(void) {
  char choice;
  
  for(;;) {
    printf("\nModify phone entry by:\n");
    printf("[1] Phone Number\n");
    printf("[2] Last Name\n");
    printf("[B] Back\n");
  
    printf("\nEnter choice: ");
    scanf("%c", &choice);
    while (getchar() != '\n') continue;
    
    switch (choice) {
      case '1':
        modify_phone_entry(find_phone_entry_by_number());
        return;
      case '2':
        modify_phone_entry(find_phone_entry_by_last_name());
        return;
      case 'b':
      case 'B':
        return;
      default:
        printf("Invalid Option\n");
    }
  }
}

/* Display the menu to delete a phone entry. Get user input 
 * for their choice and take the appropriate action.
 */
void display_delete_phone_entry_menu(void) {
  char choice;
  
  for(;;) {
    printf("\nDelete phone entry by:\n");
    printf("[1] Phone Number\n");
    printf("[2] Last Name\n");
    printf("[B] Back\n");
  
    printf("\nEnter choice: ");
    scanf("%c", &choice);
    while (getchar() != '\n') continue;
    
    switch (choice) {
      case '1':
        delete_phone_entry(find_phone_entry_by_number());
        return;
      case '2':
        delete_phone_entry(find_phone_entry_by_last_name());
        return;
      case 'b':
      case 'B':
        return;
      default:
        printf("Invalid Option\n");
    }
  }
}

/* Display the menu to delete an unused area. Get user input 
 * for their choice and take the appropriate action.
 */
void display_delete_area_menu(void) {
  char choice;
  
  for(;;) {
    printf("\nDelete area by:\n");
    printf("[1] Area Code\n");
    printf("[2] Area Name\n");
    printf("[B] Back\n");
  
    printf("\nEnter choice: ");
    scanf("%c", &choice);
    while (getchar() != '\n') continue;
    
    switch (choice) {
      case '1':
        delete_area(area_to_delete_by_area_code(NULL));
        return;
      case '2':
        delete_area(area_to_delete_by_name());
        return;
      case 'b':
      case 'B':
        return;
      default:
        printf("Invalid Option\n");
    }
  }
}
