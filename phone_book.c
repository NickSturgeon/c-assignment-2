/* PROGRAM:             phone_book.c
   AUTHOR:              Nicholas Sturgeon
   DATE:                2018/11/10
   PURPOSE:             Linked list for phone entries
   LEVEL OF DIFFICULTY: 2
   CHALLENGES:          Working with linked lists
   HOURS SPENT:         2.5 hours
*/

#include "area.h"
#include "phone_book.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Length of a formatted phone number (including null terminator). */
#define PHONE_STR_LEN 15

struct phone_entry *phone_entry_head = NULL, *phone_entry_tail = NULL;

/* Add a phone entry onto the head of the linked list, 
 * or initializes the list if it is empty.
 * Displays all the phone entries in the list.
 */
void add_phone_entry(void) {
  struct phone_entry *phone_entry = new_phone_entry();
  
  if (phone_entry == NULL) {
    printf("Invalid Phone Entry\n");
    return;
  } else if (phone_entry_head == NULL) {
    initialize_phone_list(phone_entry);
  } else {
    phone_entry_head = phone_entry;
  }
  
  display_phone_entries();
}

/* Get details for a new phone entry from user input.
 * Returns the newly created phone entry.
 */
struct phone_entry *new_phone_entry(void) {
  struct phone_entry *new_phone_entry = malloc(sizeof(struct phone_entry));
  struct area *area;
  short area_code;
  long phone_number;
  
  printf("Enter Area Code: ");
  scanf("%hd", &area_code);
  area = find_area_by_area_code(area_code);
  if (area == NULL) {
    printf("Area Code Not Found\n");
    while (getchar() != '\n') continue;
    return NULL;
  } else new_phone_entry->area = area;
  
  printf("Enter Phone Number: ");
  scanf("%ld", &phone_number);
  if (check_phone_number_length(phone_number)) {
    printf("Invalid Phone Number\n");
    while (getchar() != '\n') continue;
    return NULL;
  } else {
    new_phone_entry->phone_number = phone_number;
  }
  
  printf("Enter First Name: ");
  scanf("%s", new_phone_entry->first_name);
  printf("Enter Last Name: ");
  scanf("%s", new_phone_entry->last_name);
  
  new_phone_entry->next_phone_entry = phone_entry_head;
  
  while (getchar() != '\n') continue;
  return new_phone_entry;
}

/* Initializes an empty list by assigning the head and
 * tail to the specified phone entry.
 */
void initialize_phone_list(struct phone_entry *phone_entry) {
  phone_entry_head = phone_entry;
  phone_entry_tail = phone_entry;
}

/* Modifies a phone entry in the list to new values from
 * user input. Validates that the phone entry exists.
 */
void modify_phone_entry(struct phone_entry *phone_entry) {
  short area_code;
  long phone_number;
  struct area *area;
  
  if (phone_entry == NULL) {
    printf("Phone entry not found.\n");
    return;
  }
  
  printf("Enter New Area Code: ");
  scanf("%hd", &area_code);
  area = find_area_by_area_code(area_code);
  if (area == NULL) {
    printf("Area Code Not Found.\n");
    while (getchar() != '\n') continue;
    return;
  }
  
  printf("Enter New Phone Number: ");
  scanf("%ld", &phone_number);
  if (check_phone_number_length(phone_number)) {
    printf("Invalid Phone Number\n");
    return;
  }
  
  phone_entry->area = area;
  phone_entry->phone_number = phone_number;
  
  printf("Enter New First Name: ");
  scanf("%s", phone_entry->first_name);
  printf("Enter New Last Name: ");
  scanf("%s", phone_entry->last_name);
  
  while (getchar() != '\n') continue;
  
  printf("Modification Successful\n");
  display_phone_entries();
}

/* Deletes a phone entry from the list by making the
 * previous node bypass it and freeing it from memory.
 */
void delete_phone_entry(struct phone_entry *phone_entry) {
  struct phone_entry *current = phone_entry_head;
  
  if (phone_entry == NULL) {
    printf("Phone entry not found.\n");
    return;
  }
  
  if (phone_entry_head == phone_entry && phone_entry_tail == phone_entry) {
    phone_entry_head = NULL;
    phone_entry_tail = NULL;
  } else if (phone_entry_head == phone_entry) {
    phone_entry_head = phone_entry->next_phone_entry;
  } else {
    while (current->next_phone_entry != phone_entry)
      current = current->next_phone_entry;
  
    current->next_phone_entry = phone_entry->next_phone_entry;
  }
  
  free(phone_entry);
  printf("Deletion Successful\n");
  display_phone_entries();
}

/* Searches the list to find a phone entry from the inputted 
 * phone number.
 * If none are found, returns NULL. Returns the matching
 * phone entry otherwise.
 */
struct phone_entry *find_phone_entry_by_number(void) {
  short area_code;
  long phone_number;
  struct phone_entry *current = phone_entry_head;
  
	printf("Enter area code: ");
	scanf("%hd", &area_code);

	printf("Enter 7-digit phone number: ");
	scanf("%ld", &phone_number);
  
  while (getchar() != '\n') continue;
  
  while (current != NULL) {
    if (current->area->area_code == area_code && current->phone_number == phone_number)
      return current;
    current = current->next_phone_entry;
  }
  
  return NULL;
}

/* Searches the list to find a phone entry from the inputted 
 * last name. If none are found, returns NULL. Returns the 
 * matching phone entry otherwise.
 */
struct phone_entry *find_phone_entry_by_last_name(void) {
  char last_name[20];
  struct phone_entry *current = phone_entry_head;
  
  printf("Enter last name: ");
  scanf("%s", last_name);
  while (getchar() != '\n') continue;
  
  while (current != NULL) {
    if (strcmp(current->last_name, last_name) == 0)
      return current;
    current = current->next_phone_entry;
  }
  
  return NULL;
}

/* Makes sure that a specified area code is 7 digits.
 * Returns 1 if it doesn't meet requirements, 0 if it does.
 */
int check_phone_number_length(long phone_number) {
  return phone_number < 1000000 || phone_number > 9999999;
}

/* Checks to see if a specified area code is
 * currently being used by a phone entry in the list.
 * Returns 1 if it is, 0 if it's not.
 */
int check_area_in_use(short area_code) {
  struct phone_entry *current = phone_entry_head;
  
  while (current != NULL) {
    if (current->area->area_code == area_code)
      return 1;
    current = current->next_phone_entry;
  }
  
  return 0;
}

/* Format a phone number to be of the form:
 * (###) ###-####
 * Returns the formatted phone number.
 */
char *format_phone_number(short area_code, long phone_number) {
	char * formatted_phone_number = (char *) malloc(PHONE_STR_LEN);
	short central_office = phone_number / 10000;
	short subscriber = phone_number % 10000;

	snprintf(formatted_phone_number, PHONE_STR_LEN, 
		"(%d) %d-%04d", area_code, central_office, subscriber
	);

	return formatted_phone_number;
}

/* Displays the info of each phone entry in the list and the
 * total count.
 */
void display_phone_entries(void) {
  struct phone_entry *current = phone_entry_head;
  short count = 0;
  char *phone_number;
  
  printf("\n");
  while (current != NULL) {
    phone_number = format_phone_number(current->area->area_code, current->phone_number);
    printf("%s\n", phone_number);
    free(phone_number);
    current = current->next_phone_entry;
    count++;
  }
  
  printf("Total Phone Entries: %d\n", count);
}
