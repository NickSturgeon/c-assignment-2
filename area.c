/* PROGRAM:             area.c
   AUTHOR:              Nicholas Sturgeon
   DATE:                2018/11/10
   PURPOSE:             Doubly linked list for areas
   LEVEL OF DIFFICULTY: 3
   CHALLENGES:          Doubly linked lists
   HOURS SPENT:         3.5 hours
*/

#include "area.h"
#include "phone_book.h"
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>

struct area *area_head = NULL, *area_tail = NULL;

/* Add an area onto the tail of the doubly linked list, 
 * or initializes the list if it is empty.
 * Displays all the areas in the list.
 */
void add_area(void) {
  struct area *area = new_area(area_tail);
  if (check_unique_area_code(area->area_code) || check_area_code_length(area->area_code)) {
    printf("Area Code Already Taken or Invalid Area Code\n");
    return;
  }
  
  if (area_head == NULL) {
    initialize_area_list(area);
  } else {
    area_tail->next_area = area;
    area_tail = area;
  }
  
  display_areas();
}

/* Get details for a new area from user input.
 * Returns the newly created area.
 */
struct area *new_area(struct area *area_tail) {
  struct area *new_area = malloc(sizeof(struct area));
  printf("Enter an area code (3 digits): ");
  scanf("%hd", &new_area->area_code);
  printf("Enter city name: ");
  scanf("%s", new_area->area_name);
  new_area->prev_area = area_tail;
  new_area->next_area = NULL;
  
  while (getchar() != '\n') continue;
  return new_area;
}

/* Initializes an empty list by assigning the head and
 * tail to the specified area.
 */
void initialize_area_list(struct area *area) {
  area_head = area;
  area_tail = area;
}

/* Gets an area in the list to delete by a specified
 * area code. Optionally supplies an area name as well
 * to match against. 
 * Returns NULL and displays an appropriate
 * message if errors are encountered.
 * Otherwise returns the area to delete.
 */
struct area *area_to_delete_by_area_code(char *name) {
  short area_code;
  struct area *current = area_head;
  
  printf("Enter an area code (3 digits): ");
  scanf("%hd", &area_code);
  while (getchar() != '\n') continue;
  
  if (check_area_in_use(area_code)) {
    printf("Area In Use\n");
    return NULL;
  }
  
  while (current != NULL) {
    if (current->area_code == area_code)
      if (name == NULL || strcmp(current->area_name, name) == 0) {
        return current;
      } else {
        printf("Area Code Does Not Belong to %s\n", name);
        return NULL;
      }
      
    current = current->next_area;
  }
  
  printf("Can't Find Area Code\n");
  return NULL;
}

/* Gets an area in the list to delete by a specified
 * area name. If multiple area codes are found for an area
 * name, additionally requests the area code.
 * Returns NULL and displays an appropriate
 * message if errors are encountered.
 * Otherwise returns the area to delete.
 */
struct area *area_to_delete_by_name(void) {
  char area_name[20];
  short count = 0;
  struct area *current = area_head;
  struct area *area = NULL;
  
  printf("Enter area name: ");
  scanf("%s", area_name);
  while (getchar() != '\n') continue;
  
  while (current != NULL) {
    if (strcmp(current->area_name, area_name) == 0) {
      count++;
      area = current;
    }
    current = current->next_area;
  }
  
  if (count == 0) {
    printf("Can't Find Any Area Codes Belonging to %s\n", area_name);
    return NULL;
  } else if (count == 1) {
    return area;
  } else {
    printf("Multiple Codes Found For %s\nPlease Specify the Area Code\n", area->area_name);
    return area_to_delete_by_area_code(area->area_name);
  }
}

/* Deletes an area from the list by making the
 * previous and next nodes bypass it and freeing it
 * from memory.
 */
void delete_area(struct area *area) {
  if (area == NULL) {
    printf("Unable To Delete Area\n");
    return;
  }
  
  if (area_head == area && area_tail == area) {
    area_head = NULL;
    area_tail = NULL;
  } else if (area_head == area) {
    area_head = area->next_area;
    area_head->prev_area = NULL;
  } else if (area_tail == area) {
    area_tail = area->prev_area;
    area_tail->next_area = NULL;
  } else {
    area->prev_area->next_area = area->next_area;
    area->next_area->prev_area = area->prev_area;
  }
  
  free(area);
  printf("Deletion Successful\n");
  display_areas();
}

/* Checks to see if a specified area code is
 * already present in the list.
 * Returns 1 if it is present, 0 if it's not.
 */
int check_unique_area_code(short area_code) {
  struct area *current = area_head;
  
  while (current != NULL) {
    if (current->area_code == area_code)
      return 1;
    current = current->next_area;
  }
  
  return 0;
}

/* Makes sure that a specified area code is 3 digits.
 * Returns 1 if it doesn't meet requirements, 0 if it does.
 */
int check_area_code_length(short area_code) {
  return area_code < 100 || area_code > 999;
}

/* Searches the list to find an area from the specified
 * area code. If none are found, returns NULL. Returns the 
 * matching area otherwise.
 */
struct area *find_area_by_area_code(short area_code) {
  struct area *current = area_head;
  
  while (current != NULL) {
    if (current->area_code == area_code)
      return current;
    
    current = current->next_area;
  }
  
  return NULL;
}

/* Displays the info of each area in the list and the
 * total count.
 */
void display_areas(void) {
  struct area *current = area_head;
  short count = 0;
  
  printf("\n");
  while (current != NULL) {
    printf("%s\n", current->area_name);
    current = current->next_area;
    count++;
  }
  
  printf("Total Areas: %hd\n", count);
}
