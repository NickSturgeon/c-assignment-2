#ifndef AREA_H
#define AREA_H 

struct area {
  short area_code;
  char area_name[20];
  struct area *prev_area;
  struct area *next_area;
};

void add_area(void);
struct area *new_area(struct area *);
void initialize_area_list(struct area *);
struct area *area_to_delete_by_area_code(char *);
struct area *area_to_delete_by_name(void);
void delete_area(struct area *);
int check_unique_area_code(short);
int check_area_code_length(short);
struct area *find_area_by_area_code(short);
void display_areas();

#endif
